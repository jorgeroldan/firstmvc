// * Nuestra informacion se encuentra aislada
const m = {
  dataText: 'mi primer mvc',
  dataHead: 'Buen trabajo!!'
}

// * nuestra vista se encarga de mostrar en panalla lo que requerimos
// * tambien se va a encargar de las interacciones desde el cliente
const v = {
  renderAlert: function(model){
    swal(model.dataHead, model.dataText, "success");
  },
  renderBody: function(model){
    const newContentText = document.getElementById('textContent')
    newContentText.innerHTML= ` <h1 class="title">${model.dataHead}</h1><h2 class="subtitle">
    ${model.dataText}</h2> `;
  },

 }

// * El controlador se ecncarga de las acciones y respuestas.
const c = {
  updateDataOnload: function(){
    v.renderBody(m);
  },
  onAlertBtnClick: function(){
    v.renderAlert(m);
  }
};

const btn = document.getElementById('alertCta');
btn.addEventListener('click', c.onAlertBtnClick);

window.onload = c.updateDataOnload;