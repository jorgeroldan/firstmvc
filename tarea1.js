// // * Nuestra informacion se encuentra aislada
const m = {
  es:{
    dataText: 'mi primer mvc',
    dataHead: 'Buen trabajo!!',
    dataButton: 'Mostrar alerta'
  },
  en:{
    dataText: 'my first mvc',
    dataHead: 'Awesome work!!',
    dataButton: 'Show alert'
  },

  // Queremos guardar en el local storage el idioma seleccionado
  verDataLocalStorage : function(){
 
    let idioma = localStorage.getItem('idioma');
    if(idioma){
      return idioma;
    }else{
      return 'en'
    }
  },
  grabarLocalStorage: function (idioma) {
    localStorage.setItem('idioma', idioma);
  }
}



// * nuestra vista se encarga de mostrar en panalla lo que requerimos
// * tambien se va a encargar de las interacciones desde el cliente


const v = {
  renderAlert: function (data) {
    swal(data.dataHead, data.dataText, "success");
    // cambiarIdioma function(){
    // } 
  },
  renderBody: function (data) {
    const newContentText = document.getElementById('textContent');
    newContentText.innerHTML = ` <h1 id="title" class="title">${data.dataHead}</h1><h2 id="subtitle" class="subtitle">
    ${data.dataText} </h2> `;

  },
  blabla: function(data){
    // let newContentButtonText = document.getElementById('alertaCta');
    // newContentButtonText.innerText = data.dataButton;
    const btn_en = document.getElementById('en');
    const btn_es = document.getElementById('es');
    // let title = document.getElementsById('title');
    // let subtitle = document.getElementsById('subtitle');
    btn_en.addEventListener('click', function () {
      m.grabarLocalStorage('en');
      c.updateDataOnload();
    });
    btn_es.addEventListener('click', function () {
      m.grabarLocalStorage('es');
      c.updateDataOnload();
    });
  },
  configButton: function (dataUsuario) {
    const repitoFuncion = this.renderAlert
    console.log(repitoFuncion)
    document.getElementById('alertCta').addEventListener("click", function (event) {
      repitoFuncion(dataUsuario)
      console.log('esto fue idea de edu')
    }, false);
  },
}
// * El controlador se ecncarga de las acciones y respuestas.
const c = {
  updateDataOnload: function(){
    const idiomaSeleccionado = m.verDataLocalStorage()
    v.configButton(m[idiomaSeleccionado]);
    let flag = true;
    if (flag){
      v.renderAlert(m[idiomaSeleccionado]);
    } 
    flag = false;
    v.renderBody(m[idiomaSeleccionado]);
    v.blabla(m[idiomaSeleccionado]);
  }

// Queremos escuchar un evento en el que el usuario da clic a uno de los botones de idoma y se modifique el modelo
  

};


window.onload = c.updateDataOnload;