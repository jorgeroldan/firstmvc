# firstMVC

## Tareas

### 13-08-2018

Agregar al mvc un sistema que permita deshabilitar la alerta al ejecutar el botón  una vez que el usuario lo presiona 3 veces en una misma sesión, si realiza esto, al siguiente ingreso, no se debe mostrar la alerta.

### 15-08-2018

Agregar al mvc un sistema de selección de idiomas, donde el usuario puede seleccionar la web en ingles o español y mostrar la información según corresponda en el siguiente ingreso.